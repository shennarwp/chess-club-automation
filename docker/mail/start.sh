#!/bin/bash

docker run \
	-p 25:25 \
	-p 143:143 \
	-p 587:587 \
	-p 993:993 \
	-v $PWD/config/:/tmp/docker-mailserver/ \
	-v $PWD/maildata/:/var/mail \
	-e ENABLE_FAIL2BAN=1 \
	--name mail \
	--hostname mail.chesscorp.org \
	-d tvial/docker-mailserver:latest

