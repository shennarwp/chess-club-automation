#!/bin/bash

for CLUBPID in $(ps -ef | grep 'java -jar ' | grep chess-club | grep -v -e grep -e sudo | awk '{print $2;}')
do
    echo "Stopping process ${CLUBPID}"
    sudo kill ${CLUBPID}
    sleep 30
done

for CLUBPID in $(ps -ef | grep 'java -jar ' | grep chess-club | grep -v -e grep -e sudo | awk '{print $2;}')
do
    echo "Killing process ${CLUBPID}"
    sudo kill -9 ${CLUBPID}
    sleep 2
done

echo "Shutdown complete"
