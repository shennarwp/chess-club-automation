#!/bin/bash

docker run \
	--name chess-club-01    \
	-p 80:8080              \
	--link chess-postgres-01:chessdb \
	-d chesscorp/chess-club
