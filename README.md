## What's here ?

In a world where DEVOPS is the new frontier... ;-)

These are a bunch of tools to automate setup, operation and upgrade of ChessCorp club instances.



## Installing a club instance using this project

### Pre-requisites

System tools:

* Git obviously

Working Java environment:

* Java Development Kit 8
* Apache Maven 3

Node JS and the following commands:

* bower
* gulp

### Setup of a club instance

Clone this project on your filesystem. Then run the **scripts/refresh.sh** to install/update the club projects.

You should end up with folders for each built project and a folder for runtime data:

* chess-club
* chess-club-automation
* chess-club-ui
* runtime


