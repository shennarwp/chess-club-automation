#!/bin/bash

for cid in $(docker ps -a| grep -v CONTAINER | awk '{print $1;}')
do
	echo "Stopping container" $( docker ps -a | grep $cid | sed -e 's/ .* / \/ /' )
	docker stop $cid >&- && echo "  Process $cid stopped"
	docker rm   $cid >&- && echo "  Process $cid removed"
done
